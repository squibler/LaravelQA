<?php

namespace Squibler\QA\Console\Commands;

use Illuminate\Console\Command;
use Squibler\QA\Support\Commands\CodeSnifferCommand;
use Squibler\QA\Support\Commands\CodeBeautifierCommand;
use Squibler\QA\Support\Commands\ParallelLinterCommand;
use Squibler\QA\Support\Commands\SecurePackagesCommand;
use Squibler\QA\Support\Commands\DocCheckCommand;
use Squibler\QA\Support\Commands\BackwardCompatibilityCommand;
use Squibler\QA\Support\Commands\StaticAnalysisCommand;
use Squibler\QA\Support\Commands\UnitTestCommand;

class QA extends Command
{
    protected $name = 'squib:qa';
    protected $signature = 'squib:qa';
    protected $description = 'Run the complete QA suite';
    protected $type = 'QA';
    protected $parsers;
    protected $commandResults = [];

    protected $actions = [
        CodeBeautifierCommand::class,
        ParallelLinterCommand::class,
        UnitTestCommand::class,
        StaticAnalysisCommand::class,
        CodeSnifferCommand::class,
        DocCheckCommand::class,
        BackwardCompatibilityCommand::class,
        SecurePackagesCommand::class,
    ];

    protected $commands = [];

    public function __construct()
    {
        parent::__construct();
        foreach($this->actions as $cmd) {
            $this->commands[$cmd] = new $cmd;
        }
    }

    public function handle()
    {
        $count = count($this->commands);
        $this->output->progressStart($count);

        foreach($this->commands as $cmd) {
            $cmd->run();
            $this->output->progressAdvance();
        }

        $this->output->progressFinish();
        $this->printResults();
    }

    private function printResults()
    {
        $headers = [
            'order'=> '',
            'act' => 'Action',
            'cmd' => 'Command',
            'passes' => 'Passed'
        ];

        $order = 1;
        $data = [];
        foreach($this->commands as $cmd => $commander) {
            // $parser = Parser::parse($commander);
            $data[] = [
                'order' => $order++,
                'act' => substr($cmd, strrpos($cmd, '\\')+1),
                'cmd' => '<info>'.$commander->command().'</info>',
                'passes' => $commander->passes()
            ];
        }

        $this->table($headers, $data);
    }
}
