<?php

namespace Squibler\QA\Providers;

use Squibler\QA\Console\Commands\QA;
use Squibler\QA\Console\Commands\Secure;
use Squibler\QA\Console\Commands\Standardise;
use Squibler\QA\Console\Commands\Standardize;
use Squibler\QA\Console\Commands\BackwardCompatibility;
use Squibler\QA\Console\Commands\Lint;
use Squibler\QA\Console\Commands\Sniff;

use Illuminate\Support\ServiceProvider;


class QAServiceProvider extends ServiceProvider
{
    const PACKAGE_ARTEFACTS_PATH = __DIR__.'/../../artefacts/';

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->commands([
            QA::class
        ]);

        $this->publishes([
            self::PACKAGE_ARTEFACTS_PATH . 'phpcs.xml' => base_path('phpcs.xml'),
        ], 'phpcs');
    }
}
