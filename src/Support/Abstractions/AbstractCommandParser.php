<?php

namespace Squibler\QA\Support\Abstractions;

use Squibler\QA\Exceptions\InvalidParserException;

abstract class AbstractCommandParser
{
    protected $parses = null;

    abstract public function parse(AbstractCommand $output): AbstractCommandParser;
    // abstract public function errors();

    public function __construct()
    {
        if (! class_exists($this->parses)) {
            throw new InvalidParserException();
        }
    }


    public function handles(string $command)
    {
        return ($command === $this->parses);
    }
}
