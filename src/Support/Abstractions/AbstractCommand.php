<?php

namespace Squibler\QA\Support\Abstractions;

use Symfony\Component\Process\Process;
use Squibler\QA\Exceptions\InvalidCommandException;

abstract class AbstractCommand
{
    protected $command;
    protected $process;

    public function __construct()
    {
        if (method_exists($this, 'setup')) {
            $this->setup();
        }

        if (! $this->command) {
            throw new InvalidCommandException();
        }
    }


    public function run()
    {
        $this->process = new Process($this->command, base_path());
        $this->process->run();
        return $this;
    }


    public function command()
    {
        return $this->command;
    }


    public function output()
    {
        return ($this->process->getErrorOutput())
            ? $this->process->getErrorOutput()
            : $this->process->getOutput();
    }


    public function called()
    {
        return get_called_class();
    }


    public function passes()
    {
        return ($this->process->isSuccessful())
            ? '<info>YES </info>'
            : '<error> NO </error>';
    }
}
