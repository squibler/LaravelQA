<?php

namespace Squibler\QA\Support\Commands;

use Squibler\QA\Support\Abstractions\AbstractCommand;

class BackwardCompatibilityCommand extends AbstractCommand
{
    protected $command = './vendor/bin/roave-backward-compatibility-check';
}
