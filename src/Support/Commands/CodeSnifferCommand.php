<?php

namespace Squibler\QA\Support\Commands;

use Symfony\Component\Process\Process;
use Squibler\QA\Support\Abstractions\AbstractCommand;

class CodeSnifferCommand extends AbstractCommand
{
    protected function setup()
    {
        $options = [];
        $this->command = sprintf(
            './vendor/bin/phpcs --report=json %s',
            join(' ', $options)
        );
    }
}
