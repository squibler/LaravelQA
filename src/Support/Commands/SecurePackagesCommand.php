<?php

namespace Squibler\QA\Support\Commands;

use Squibler\QA\Support\Abstractions\AbstractCommand;

class SecurePackagesCommand extends AbstractCommand
{
    protected $command = 'php artisan security-check:now';
}
