<?php

namespace Squibler\QA\Support\Commands;

use Squibler\QA\Support\Abstractions\AbstractCommand;

class CodeBeautifierCommand extends AbstractCommand
{
    protected function setup()
    {
        $options = [];
        $this->command = sprintf(
            './vendor/bin/phpcbf --report=json %s',
            join(' ', $options)
        );
    }
}
