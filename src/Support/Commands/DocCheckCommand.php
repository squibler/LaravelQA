<?php

namespace Squibler\QA\Support\Commands;

use Squibler\QA\Support\Abstractions\AbstractCommand;

class DocCheckCommand extends AbstractCommand
{
    protected $command = './vendor/bin/php-doc-check --exclude=./vendor .';
}
