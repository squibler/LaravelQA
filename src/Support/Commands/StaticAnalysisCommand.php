<?php

namespace Squibler\QA\Support\Commands;

use Squibler\QA\Support\Abstractions\AbstractCommand;

class StaticAnalysisCommand extends AbstractCommand
{
    protected $command = './vendor/bin/phpstan analyse -l 4 app tests packages';
}
