<?php

namespace Squibler\QA\Support\Commands;

use Squibler\QA\Support\Abstractions\AbstractCommand;

class UnitTestCommand extends AbstractCommand
{
    protected $command = './vendor/bin/phpunit';
}
