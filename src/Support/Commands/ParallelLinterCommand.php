<?php

namespace Squibler\QA\Support\Commands;

use Squibler\QA\Support\Abstractions\AbstractCommand;

class ParallelLinterCommand extends AbstractCommand
{
    protected $command = './vendor/bin/parallel-lint --exclude ./vendor .';
}
