<?php

namespace Squibler\QA\Support\Parsers;

use Squibler\QA\Support\Abstractions\AbstractCommand;

class NullParser
{
    public function parse(AbstractCommand $command)
    {
        return $this;
    }

    public function commandSucceeded()
    {
        return '';
    }
}
