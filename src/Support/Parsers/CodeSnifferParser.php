<?php

namespace Squibler\QA\Support\Parsers;

use Squibler\QA\Support\Abstractions\AbstractCommandParser;
use Squibler\QA\Support\Commands\CodeSnifferCommand;
use Squibler\QA\Support\Abstractions\AbstractCommand;

class CodeSnifferParser extends AbstractCommandParser
{
    protected $decoded;
    protected $parses = CodeSnifferCommand::class;

    public function parse(AbstractCommand $command): AbstractCommandParser
    {
        $json = null;
        preg_match('/\{\"totals.+\}{3}/', $command->output(), $json);
        $this->decoded = json_decode($json[0]);
        return $this;
    }
}
