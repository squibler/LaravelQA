<?php

namespace Squibler\QA\Support;

use Squibler\QA\Support\Abstractions\AbstractCommand;
use Squibler\QA\Support\Abstractions\AbstractCommandParser;
use Squibler\QA\Support\Parsers\NullParser;

class Parser
{
    public static function parse(AbstractCommand $command)
    {
        $parsers = self::getParsers();
        foreach ($parsers as $parser) {
            if (! $parser->handles($command->called())) {
                continue;
            }
            return $parser->parse($command);
        }
        return (new NullParser)->parse($command);
    }

    private static function getParsers()
    {
        static $parsers = [];
        if (empty($parsers)) {
            foreach (glob(__DIR__ . '/Parsers/*Parser.php') as $filename) {
                $parser = basename($filename, '.php');
                if ($parser === 'NullParser') {
                    continue;
                }

                $class = '\\Squibler\\QA\\Support\\Parsers\\' . $parser;
                if (! is_subclass_of($class, AbstractCommandParser::class)) {
                    continue;
                }

                $parsers[] = new $class;
            }
        }
        return $parsers;
    }
}
