# Description

QA Your project with a single artisan command

LaravelQA stands on the shoulders of Giants, Tools installed to Composer --dev

- beyondcode/laravel-dump-server
- roave/backward-compatibility-check
- squizlabs/php_codesniffer
- niels-de-blaauw/php-doc-check
- jakub-onderka/php-parallel-lint
- jorijn/laravel-security-checker
- nunomaduro/larastan

Also uses  the bundled PHPUnit

-

## Usage

## 1. Install as dev dependency using composer

```bash
composer require --dev squibler/laravel-qa
```

## 2. QA Your Laravel Project

```bash
$ php artisan squib:qa
 8/8 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

+---+------------------------------+------------------------------------------------------+--------+
|   | Action                       | Command                                              | Passed |
+---+------------------------------+------------------------------------------------------+--------+
| 1 | CodeBeautifierCommand        | ./vendor/bin/phpcbf --report=json                    | YES    |
| 2 | ParallelLinterCommand        | ./vendor/bin/parallel-lint --exclude ./vendor .      | YES    |
| 3 | UnitTestCommand              | ./vendor/bin/phpunit                                 | YES    |
| 4 | StaticAnalysisCommand        | ./vendor/bin/phpstan analyse -l 4 app tests packages | NO     |
| 5 | CodeSnifferCommand           | ./vendor/bin/phpcs --report=json                     | YES    |
| 6 | DocCheckCommand              | ./vendor/bin/php-doc-check --exclude=./vendor .      | YES    |
| 7 | BackwardCompatibilityCommand | ./vendor/bin/roave-backward-compatibility-check      | NO     |
| 8 | SecurePackagesCommand        | php artisan security-check:now                       | YES    |
+---+------------------------------+------------------------------------------------------+--------+
```

